# Email Templates

This project contains responsive email templates commonly used for University of Michigan emails. There are currently three templates, along with the images used in each one. Each have been updated to use more modern methods of CSS and HTML.

## Default

**default.html** is the most used template. 

### Elements

- Header with linked logo
- Banner image
- Headline
- Content
- Call to action button
- Highlighted content area
- Columned content
- Footer with social media icons and a disclaimer

### Demo

https://umichemail.org/_TEMPLATES/default.html

## Bicentennial

**bicentennial.html** is similar to **default.html**, but styled like the original Bicentennial branded emails. 

### Elements

- Header with linked logo
- Banner image
- Headline
- Content
- Call to action button
- Events listing
- Columned content
- Footer with social media icons and a disclaimer

### Demo

https://umichemail.org/_TEMPLATES/bicentennial.html

## Simple

**simple.html** is the template commonly used by the President. 

### Elements

- Linked banner image
- Headline
- Content
- Footer with linked logo
- Disclaimer

### Demo

https://umichemail.org/_TEMPLATES/simple.html

# MailChimp

## Preview Text

Replace 


```html
<!-- START PREHEADER -->
<div style="display: none; font-size: 0px; line-height: 0px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; visibility: hidden; mso-hide: all;">
    Insert your preheader text.    
</div>
<!-- END PREHEADER -->

<!-- START PREVIEW TEXT HACK - http://litmus.com/blog/the-little-known-preview-text-hack-you-may-want-to-use-in-every-email -->
<div style="display: none; font-size: 0px; line-height: 0px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; visibility: hidden; mso-hide: all;">
    &nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
</div>
<!-- END PREVIEW TEXT HACK -->
```
with

```html
<!--*|IF:MC_PREVIEW_TEXT|*-->
<!--[if !gte mso 9]><!-->
<div class="mcnPreviewText" style="display:none;font-size:0px;line-height:0px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;visibility:hidden;mso-hide:all;">*|MC_PREVIEW_TEXT|*</div>
<div style="display:none;font-size:0px;line-height:0px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;visibility:hidden;mso-hide:all;">&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;</div>
<!--<![endif]-->
<!--*|END:IF|*-->
```

Add to styles:

```css
.mcnPreviewText{
    display:none !important;
}
```